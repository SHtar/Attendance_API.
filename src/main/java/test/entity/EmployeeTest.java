package test.entity;

import eic.entity.EmployeeEntity;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2015/7/5 0005.
 */
public class EmployeeTest {
    SessionFactory sf;

    @Before
    public void before()
    {
        sf = new Configuration().configure().buildSessionFactory();
    }

    @Test
    public void test()
    {
        EmployeeEntity ee = (EmployeeEntity) sf.openSession().get(EmployeeEntity.class,889);
        Assert.assertNotNull(ee);
        System.out.println("name:" + ee.getName() + "deparment:" + ee.getDepartment().getName());
    }


}

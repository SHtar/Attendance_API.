package test.entity;

import eic.entity.BusinessEntity;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2015/7/5 0005.
 */
public class BusinessTest {
    SessionFactory sf ;

    @Before
    public void before()
    {
        sf = new Configuration().configure().buildSessionFactory();
    }

    @Test
    public void testEntity()
    {
        BusinessEntity be = (BusinessEntity) sf.openSession().get(BusinessEntity.class, 1);
       Assert.assertNotNull( be);
        System.out.println();
    }
}

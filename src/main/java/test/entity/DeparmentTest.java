package test.entity;

import eic.entity.DepartmentEntity;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2015/7/5 0005.
 */
public class DeparmentTest{
    SessionFactory sf ;

    @Before
    public void before()
    {
        sf=new Configuration().configure().buildSessionFactory();
    }

    @Test
    public void department() {
        DepartmentEntity de= (DepartmentEntity) sf.openSession().get(DepartmentEntity.class,1);
        System.out.println("name:" + de.getName() + " business:" + de.getBusinessEntities().getName());
        Assert.assertNotNull(de);
    }
}

package test.entity;

import eic.entity.ClientEntity;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by lzj on 2015/7/6.
 */
public class ClientTest {
    SessionFactory sf ;

    @Before
    public void before()
    {
        sf=new Configuration().configure().buildSessionFactory();
    }

    @Test
    public void client()
    {
        ClientEntity client= (ClientEntity) sf.openSession().get(ClientEntity.class,1);
        System.out.println("name:" + client.getName() + " address:" +client.getAddress());
        Assert.assertNotNull(client);

    }
}

package test.entity;

import eic.entity.EmployeeEntity;
import eic.entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Tang on 2015/7/5.
 */
public class UserTest {

    SessionFactory sessionFactory ;
    Session session ;

    @Before
    public void before(){

        sessionFactory = new Configuration().configure().buildSessionFactory();
        session = sessionFactory.openSession() ;

    }

    @Test
    public void testUserEntity(){

        UserEntity entity = (UserEntity) session.get(UserEntity.class,1);
        System.out.println(entity.getAppkey());
        EmployeeEntity employeeEntity = entity.getEmployeeEntity();
        System.out.println(employeeEntity.getName());
    }

}

package test.entity;

import eic.entity.AdminEntity;
import eic.entity.JobTypeEntity;
import eic.entity.StationEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.Before;
import org.junit.Test;
import utils.JsonTool;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Tang on 2015/7/5.
 */
public class MappingTest {

    SessionFactory sessionFactory ;
    Session session ;

    @Before
    public void before(){

        sessionFactory = new Configuration().configure().buildSessionFactory();
        session = sessionFactory.openSession() ;

    }

    @Test
    public void testAmdin(){

        AdminEntity entity = (AdminEntity) session.get(AdminEntity.class,1);
        System.out.println(entity.getStringId());

    }

    @Test
    public void testJobTypesAndStation(){

        JobTypeEntity entity = (JobTypeEntity) session.get(JobTypeEntity.class,1);
        System.out.println(entity.getJobName());
        StationEntity stationEntity = entity.getStationEntity();
        System.out.println(stationEntity.getStationName());
    }

    @Test
    public void testJsonTool(){

        Map<String , Integer> map = new HashMap<String, Integer>();
        map.put("1",2);
        map.put("2",2);
        System.out.println(JsonTool.mapToJsonString(null));

    }

}

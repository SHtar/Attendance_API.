package test.dao;

import eic.dao.EmployeeDAO;
import eic.dao.impl.EmployeeImpl;
import eic.entity.EmployeeEntity;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.*;

/**
 * Created by Tang on 2015/7/5.
 */
public class EmployeeTest {


    EmployeeDAO dao = new EmployeeImpl();

    @Test
    public void test1(){

        // EmployeeImpl.getUnregisterList()

        dao = new EmployeeImpl();
        List list = dao.getUnregisterList();
        Iterator iterator = list.iterator() ;
        while (iterator.hasNext()){

            Object[] o = (Object[]) iterator.next();
            Integer id = (Integer) o[0];
            String name = (String ) o[1];

            System.out.println(id);
            System.out.println(name);
        }

    }


    @Test
    public void test2(){

        Assert.assertTrue(dao.isExistEmployee(1));

    }

    @Test
    public void test3(){
        Assert.assertTrue(dao.registerEmployee(888));
    }


    @Test
    public void test4()
    {
        List<EmployeeEntity> list = dao.getEmployeeByName("李一");
        assert list.size() >= 2;
    }

    @Test
    public void test5()
    {
        List<EmployeeEntity> list = dao.getEmployeeByDepartment(1);
        assert list.size() >= 2;
    }

}

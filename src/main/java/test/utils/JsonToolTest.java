package test.utils;

import eic.dao.EmployeeDAO;
import eic.dao.impl.EmployeeImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utils.JsonTool;

import java.awt.*;
import java.util.*;

/**
 * JsonTool Tester.
 */
public class JsonToolTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: mapToJsonString(Map map)
     */
    @Test
    public void testMapToJsonString() throws Exception {

    }

    /**
     * Method: listToJsonString(List list)
     */
    @Test
    public void testListToJsonString() throws Exception {

        EmployeeDAO dao = new EmployeeImpl();
        java.util.List list = dao.getUnregisterList();
        System.out.println(JsonTool.listToJsonString(list));

    }


} 

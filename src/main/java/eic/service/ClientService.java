package eic.service;

import eic.dao.ClientDAO;
import eic.dao.impl.ClientImpl;
import eic.entity.ClientEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by lzj on 2015/7/5.
 */
public class ClientService {
    ClientDAO clientDAO=new ClientImpl();

    /**
     * 获取所有客户
     * @return List<ClientEntity>
     */
    public List<ClientEntity> getAllClient(){
        List<ClientEntity> Clients = clientDAO.getClients();
        return Clients;
    }

    /**
     *添加客户 code成功返回200，失败返回404
     * @param client
     * @return Map<String.Object>
     */
    public Map<String,Object> addClient(ClientEntity client){
        Map<String , Object> map = new HashMap<String, Object>();
        boolean result=clientDAO.addClient(client);
        if (result){
            map.put("code",200);
        }else{
            map.put("code",500);
        }
        return map;
    }

    /**
     * 删除用户 code成功返回200，失败返回404
     * @param client
     * @return Map<String,Object>
     */
    public Map<String,Object> delClient(ClientEntity client){
        Map<String , Object> map = new HashMap<String, Object>();
        Boolean result=clientDAO.delClient(client);
        if (result){
            map.put("code",200);
        }else{
            map.put("code",500);
        }
        return map;
    }

    public Map<String,Object> updClient(ClientEntity client){
        Map<String , Object> map = new HashMap<String, Object>();
        Boolean result=clientDAO.updClient(client);
        if (result){
            map.put("code",200);
        }else{
            map.put("code",500);
        }
        return map;
    }
    public ClientEntity getClientById(int id){
        return clientDAO.getClientById(id);
    }
}

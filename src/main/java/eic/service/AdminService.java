package eic.service;

import com.sun.org.apache.bcel.internal.generic.NEW;
import eic.dao.AdminDAO;
import eic.dao.impl.AdminImpl;
import eic.entity.AdminEntity;
import utils.AppKeyCreator;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Tang on 2015/7/5.
 */
public class AdminService {

    AdminDAO adminDAO =  new AdminImpl();

    public Map<String,Object> auth(String phone , String password)
    {
        Map<String,Object> map = new HashMap<String, Object>();
        AdminEntity entity = adminDAO.getAdminEntityByPhone(phone);
        if(entity==null){
            map.put("code","404");
            map.put("error","该账户不存在!");
        }else{
            String psw = entity.getPassword();
            if (psw.equals(password)){
                map.put("code",200);
                map.put("id",entity.getId());
                // 设置账户APPKEY
                String appkey = AppKeyCreator.create(entity.getStringId());
                try {
                    adminDAO.setAppKey(entity.getId(),appkey);
                    map.put("appkey",appkey);
                } catch (Exception e) {
                    e.printStackTrace();
                    map = new HashMap<String, Object>();
                    map.put("code","500");
                    map.put("error","服务器错误！");
                }

            }else{
                map.put("code",403);
                map.put("error","密码错误!");
            }
        }
        return map ;
    }
}

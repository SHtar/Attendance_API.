package eic.mvc;

import eic.entity.JobTypeEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class HelloController {

	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello!");
		return "hello";
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	JobTypeEntity getInfo(){

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();

		JobTypeEntity entity = (JobTypeEntity) session.get(JobTypeEntity.class,1);
		System.out.println(entity.getId());
		System.out.println(entity.getJobName());
		return entity ;

	}

}
package eic.mvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eic.service.AdminService;
import eic.service.UserAuthService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import utils.JsonTool;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by Tang on 2015/7/5.
 */

@Controller
@RequestMapping("/auth")
public class AuthController {


    // 管理员登录接口
    @ResponseBody
    @RequestMapping(value = "/admins",method = RequestMethod.GET)
    public String getLoginRequest(HttpServletRequest req , HttpServletResponse resp){

        String phone = req.getParameter("phone");
        String password = req.getParameter("password");
        AdminService service = new AdminService();
        Map<String , Object> map = service.auth(phone,password);
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    //  返回允许注册的用户名
    @ResponseBody
    @RequestMapping(value = "/waitForRegisters",method = RequestMethod.GET)
    public String getRegisterUserInfo(HttpServletRequest request){

        UserAuthService userAuthService = new UserAuthService();
        return userAuthService.getUnregisterList() ;

    }

    // 注册用户
    @ResponseBody
    @RequestMapping(value = "/users" , method = RequestMethod.POST)
    public String resigerUser(HttpServletRequest request ){

        String phone = request.getParameter("phone");
        String password = request.getParameter("password");
        String id = request.getParameter("id");
        UserAuthService service = new UserAuthService();
        return service.registerUser(phone,password,id);

    }


}

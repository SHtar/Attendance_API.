package eic.mvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eic.entity.ClientEntity;
import eic.service.ClientService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import utils.ISOtoUTF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Created by lzj on 2015/7/5.
 */
@Controller
@RequestMapping("/clients")
public class ClientController {
    /**
     * 获取客户
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public String getAllClients(){
        ClientService clientService=new ClientService();
        //获取结果信息以json格式返回
        List<ClientEntity> clients =clientService.getAllClient();
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(clients);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 添加客户
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public String addClient(HttpServletRequest req,HttpServletResponse response) throws UnsupportedEncodingException {

        ClientEntity client=new ClientEntity();
        //获取参数
        String name= ISOtoUTF.changeUTF(req.getParameter("name"));
        String project=ISOtoUTF.changeUTF(req.getParameter("project"));
        String address = ISOtoUTF.changeUTF(req.getParameter("address"));
        double positionx = Double.valueOf(req.getParameter("positionx"));
        double positiony = Double.valueOf(req.getParameter("positiony"));
        //把参数赋值给客户
        client.setName(name);
        client.setAddress(address);
        client.setProject(project);
        client.setPositionX(positionx);
        client.setPositionY(positiony);
        ClientService clientService = new ClientService();
        //获取结果信息以json格式返回
        Map map =clientService.addClient(client);
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 删除客户
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.DELETE)
    public  String delClient(HttpServletRequest req){
        //获取id参数
        int id =Integer.parseInt(req.getParameter("id"));
        ClientService clientService=new ClientService();
        //根据id获取客户
        ClientEntity client =clientService.getClientById(id);
        //获取结果信息以json格式返回
        Map map =clientService.delClient(client);;
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.PUT)
    public String updClient(HttpServletRequest req) throws UnsupportedEncodingException {
        //获取id参数
        int id =Integer.parseInt(req.getParameter("id"));
        //获取其他参数
        String name= ISOtoUTF.changeUTF(req.getParameter("name"));
        String project=ISOtoUTF.changeUTF(req.getParameter("project"));
        String address = ISOtoUTF.changeUTF(req.getParameter("address"));
        double positionx = Double.valueOf(req.getParameter("positionx"));
        double positiony = Double.valueOf(req.getParameter("positiony"));
        //实例化ClientService
        ClientService clientService=new ClientService();
        //根据id获取客户
        ClientEntity client =clientService.getClientById(id);
        //更新client
        client.setName(name);
        client.setAddress(address);
        client.setProject(project);
        client.setPositionX(positionx);
        client.setPositionY(positiony);
        //获取解雇信息以json格式返回
        Map map =clientService.updClient(client);;
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;


    }


}

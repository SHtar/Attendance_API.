package eic.dao.impl;

import eic.dao.EmployeeDAO;
import eic.entity.DepartmentEntity;
import eic.entity.EmployeeEntity;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Tang on 2015/7/5.
 */
public class EmployeeImpl implements EmployeeDAO {

    SessionFactory sessionFactory = null ;
    Session session = null ;

    public EmployeeImpl(){
        sessionFactory = new Configuration().configure().buildSessionFactory();
        session = sessionFactory.openSession();
    }

    /**
     * 返回允许注册的用户
     * @return 用户名List
     */
    @Override
    public List getUnregisterList() {

        Query query = session.createQuery("select o.id , o.name from EmployeeEntity o where o.isRegister = 0 ");
        try {
            List list = query.list();
            return list ;
        }catch (Exception e){
            e.printStackTrace();
            return null ;
        }

    }

    @Override
    public boolean isExistEmployee(int id) {
        try {
            Session session = sessionFactory.openSession();
            EmployeeEntity entity = (EmployeeEntity) session.get(EmployeeEntity.class, id);
            if(entity!=null) {
                session.close();
                return true;
            }
        }catch (Exception e){
            return false ;
        }
        return false ;
    }

    @Override
    public boolean registerEmployee(int id) {

        try {
            Session session = sessionFactory.openSession() ;
            EmployeeEntity entity = (EmployeeEntity) session.load(EmployeeEntity.class,id);
            Transaction transaction = session.beginTransaction() ;
            Byte t = new Byte("1");
            entity.setIsRegister(t);
            session.save(entity);
            transaction.commit();
            session.close();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public List<EmployeeEntity> getEmployeeByName(String name) {
        List list = null;
        Session session = null;

        try
        {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(EmployeeEntity.class);
            //根据名字 筛选 员工
            criteria.add(Restrictions.eq("name",name));
            list = criteria.list();
            session.close();
        }catch (Exception e)
        {
            if(session != null) session.close();
            e.printStackTrace();

        }
        return list;
    }

    @Override
    public List<EmployeeEntity> getEmployeeByDepartment(int department_id) {
        List list = null;
        Session session = null;
        try
        {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(EmployeeEntity.class);
            //根据部门 筛选 员工

            DepartmentEntity departmentEntity = (DepartmentEntity) session.get(DepartmentEntity.class, department_id);
            criteria.add(Restrictions.eq("department" , departmentEntity));
            list = criteria.list();
            session.close();
        }catch (Exception e)
        {
            if(session != null) session.close();
            e.printStackTrace();

        }
        return list;
    }
}

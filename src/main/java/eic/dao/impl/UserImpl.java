package eic.dao.impl;

import eic.dao.UserDAO;
import eic.entity.EmployeeEntity;
import eic.entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by Tang on 2015/7/6.
 */
public class UserImpl implements UserDAO {

    SessionFactory sessionFactory = null ;
    Session session = null ;

    public UserImpl(){
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    @Override
    public boolean addUser(String phone, String password, String id , String appkey) {
        session = sessionFactory.openSession();
        try {

            EmployeeEntity employeeEntity = (EmployeeEntity) session.load(EmployeeEntity.class,Integer.valueOf(id));

            UserEntity entity = new UserEntity();
            entity.setAppkey(appkey);
            entity.setPhone(phone);
            entity.setPassword(password);
            entity.setEmployeeEntity(employeeEntity);
            Transaction transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
            return true ;
        }catch (Exception e){
            e.printStackTrace();
            return false ;
        }
    }
}

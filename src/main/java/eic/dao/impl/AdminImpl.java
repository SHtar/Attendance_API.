package eic.dao.impl;

import eic.dao.AdminDAO;
import eic.entity.AdminEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistryBuilder;

import javax.management.Query;

/**
 * Created by Tang on 2015/7/5.
 */
public class AdminImpl implements AdminDAO{

    SessionFactory sessionFactory = null ;
    Session session ;

    public AdminImpl(){
        ServiceRegistryBuilder srb = new ServiceRegistryBuilder();
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    @Override
    public AdminEntity getAdminEntityByPhone(String phone) {
        session = sessionFactory.openSession();
        AdminEntity entity = null ;
        Criteria criteria = session.createCriteria(AdminEntity.class);
        criteria.add(Restrictions.eq("phone",phone));
        entity = (AdminEntity) criteria.uniqueResult();
        session.close();
        return entity;
    }

    @Override
    public void setAppKey(int id ,String key) throws Exception{

        try {
            session = sessionFactory.openSession();
            AdminEntity entity = (AdminEntity) session.get(AdminEntity.class,id);
            entity.setAppkey(key);
            Transaction transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();

        }catch (Exception e){
            e.printStackTrace();
            throw e ;
        }

    }


}

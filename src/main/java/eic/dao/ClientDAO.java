package eic.dao;

import eic.entity.ClientEntity;

import java.util.List;

/**
 * Created by lzj on 2015/7/5.
 */
public interface ClientDAO {
    /**
     * 获取所有客户
     * @return List<ClientEntity>
     */
    public List<ClientEntity> getClients();

    /**
     * 添加客户
     * @param client
     * @return
     */
    public boolean addClient(ClientEntity client);

    /**
     * 删除客户
     * @param client
     * @return
     */
    public boolean delClient(ClientEntity client);

    /**
     * 更新客户
     * @param client
     * @return
     */
    public boolean updClient(ClientEntity client);

    /**
     * 根据客户id获取客户
     * @param id
     * @return
     */
    public ClientEntity getClientById(int id);
}

package eic.dao;

import eic.entity.EmployeeEntity;

import java.util.List;

/**
 * Created by Tang on 2015/7/5.
 */
public interface EmployeeDAO {

    public List getUnregisterList();
    public boolean isExistEmployee(int id);
    public boolean registerEmployee(int id);

    /**
     * 根据名字 获取 员工
     * @param name 员工的名字
     * @return 员工列表
     */
    public List<EmployeeEntity> getEmployeeByName(String name);

    /**
     * 根据部门 获取 员工
     * @param department_id 部门ID
     * @return 员工列表
     */
    public List<EmployeeEntity> getEmployeeByDepartment(int department_id);



}
package eic.dao;

import eic.entity.AdminEntity;

import java.util.Map;

/**
 * Created by Tang on 2015/7/5.
 */
public interface AdminDAO {




    public AdminEntity getAdminEntityByPhone(String phone);
    public void setAppKey(int id , String key) throws Exception;

}

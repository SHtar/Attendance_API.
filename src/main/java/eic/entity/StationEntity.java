package eic.entity;

import java.util.Set;

/**
 * Created by Tang on 2015/7/5.
 */
public class StationEntity {
    private int id;
    private String stationName;
    private Set<JobTypeEntity> jobTypeEntities ;

    public Set<JobTypeEntity> getJobTypeEntities() {
        return jobTypeEntities;
    }

    public void setJobTypeEntities(Set<JobTypeEntity> jobTypeEntities) {
        this.jobTypeEntities = jobTypeEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StationEntity that = (StationEntity) o;

        if (id != that.id) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        return result;
    }
}

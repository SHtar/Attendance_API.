package eic.entity;

import java.util.Set;

/**
 * Created by Administrator on 2015/7/5 0005.
 */
public class DepartmentEntity {
    private int id;
    private String name;
    private String counterman;
    private String countermanPhone;
    private String countermanMobile;
    private Integer number;
    private Integer highPositionNumber;
    private String introduction;
    private BusinessEntity businessEntities ;

    public BusinessEntity getBusinessEntities() {
        return businessEntities;
    }

    public void setBusinessEntities(BusinessEntity businessEntities) {
        this.businessEntities = businessEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCounterman() {
        return counterman;
    }

    public void setCounterman(String counterman) {
        this.counterman = counterman;
    }

    public String getCountermanPhone() {
        return countermanPhone;
    }

    public void setCountermanPhone(String countermanPhone) {
        this.countermanPhone = countermanPhone;
    }

    public String getCountermanMobile() {
        return countermanMobile;
    }

    public void setCountermanMobile(String countermanMobile) {
        this.countermanMobile = countermanMobile;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getHighPositionNumber() {
        return highPositionNumber;
    }

    public void setHighPositionNumber(Integer highPositionNumber) {
        this.highPositionNumber = highPositionNumber;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartmentEntity that = (DepartmentEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (counterman != null ? !counterman.equals(that.counterman) : that.counterman != null) return false;
        if (countermanPhone != null ? !countermanPhone.equals(that.countermanPhone) : that.countermanPhone != null)
            return false;
        if (countermanMobile != null ? !countermanMobile.equals(that.countermanMobile) : that.countermanMobile != null)
            return false;
        if (number != null ? !number.equals(that.number) : that.number != null) return false;
        if (highPositionNumber != null ? !highPositionNumber.equals(that.highPositionNumber) : that.highPositionNumber != null)
            return false;
        if (introduction != null ? !introduction.equals(that.introduction) : that.introduction != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (counterman != null ? counterman.hashCode() : 0);
        result = 31 * result + (countermanPhone != null ? countermanPhone.hashCode() : 0);
        result = 31 * result + (countermanMobile != null ? countermanMobile.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (highPositionNumber != null ? highPositionNumber.hashCode() : 0);
        result = 31 * result + (introduction != null ? introduction.hashCode() : 0);
        return result;
    }
}

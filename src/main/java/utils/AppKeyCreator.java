package utils;

import java.util.Date;

/**
 * Created by Tang on 2015/7/5.
 */
public class AppKeyCreator {

    /**
     * 生成APPKEY
     * @param id 用户ID
     * @return 返回APPKEY
     */
    public static String create(String id){
        String timeStamp = String.valueOf(new Date().getTime());
        return id + timeStamp ;
    }

}

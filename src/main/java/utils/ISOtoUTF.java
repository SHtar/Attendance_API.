package utils;

import java.io.UnsupportedEncodingException;

/**
 * Created by lzj on 2015/7/6.
 */
public class ISOtoUTF {
    public static String changeUTF(String s) throws UnsupportedEncodingException {
        String utf=new String(s.getBytes("ISO-8859-1"), "UTF-8");
        return utf;
    }
}

package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by Tang on 2015/7/5.
 */
public class JsonTool {

    /**
     * Convert Map Object to JSON String
     * @param map 要转换的Map对象
     * @return 返回JSON字符串，如果转换失败则返回NULL
     */
    public static String mapToJsonString(Map map){

        if(map==null) return null ;

        String json = null ;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null ;
        }
        return json ;
    }

    public static String listToJsonString(List list){

        if(list==null) return null ;

        String json = null ;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null ;
        }

        return json ;
    }



}
